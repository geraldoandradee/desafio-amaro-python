#!/usr/bin/env python
# ! -*- coding:utf-8 -*-

import unittest

from desafio import build_header, build_rows, read_json1, read_json2, transform


class JsonTransformTestCase(unittest.TestCase):
    def setUp(self):
        self.data = read_json1()
        self.result = read_json2()

    def test_if_header_transform_works(self):
        result = build_header(self.data)
        self.assertListEqual(["TAMANHO", "OMBROS", "BUSTO", "CINTURA", "COMPRIMENTO"], result,
                             msg='Listas nao podem ser diferentes')

    def test_if_rows_transform_works(self):
        result = build_rows(self.data)

        self.assertIsNotNone(result, msg="Linhas nao podem ser None, precisa ser um dict")
        self.assertTrue(isinstance(result, dict), msg="Linhas precisam ser um dict")

        self.assertTrue("P" in result, msg="Chave P precisa existir")
        self.assertListEqual(["P", "60 cm", "100 cm", "100 cm", "95 cm"], result['P'], msg='Lista P esta diferente do '
                                                                                           'esperado')

        self.assertTrue("M" in result, msg="Chave M precisa existir")
        self.assertListEqual(["M", "62 cm", "104 cm", "104 cm", "96 cm"], result['M'], msg='Lista M esta diferente do '
                                                                                           'esperado')

        self.assertTrue("G" in result, msg="Chave G precisa existir")
        self.assertListEqual(["G", "63 cm", "110 cm", "110 cm", "97 cm"], result['G'], msg='Lista G esta diferente do '
                                                                                           'esperado')

        self.assertTrue("GG" in result, msg="Chave GG precisa existir")
        self.assertListEqual(["GG", "64 cm", "114 cm", "114 cm", "98 cm"], result['GG'], msg='Lista GG esta diferente '
                                                                                             'do esperado')

    def test_transform(self):
        self.assertDictEqual(self.result, transform(), msg='O resultado final está diferente do esperado')
