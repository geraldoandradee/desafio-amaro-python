#!/usr/bin/env python
#! -*- coding:utf-8 -*-
import json

def read_file(filename):
    f = open(filename, 'r')
    data = f.read()
    f.close()
    return data

def read_json1():
    return {"medidas":[{"desc_medida":"OMBROS","tamanho":"G","valor":63},{"desc_medida":"OMBROS","tamanho":"GG","valor":64},{"desc_medida":"OMBROS","tamanho":"M","valor":62},{"desc_medida":"OMBROS","tamanho":"P","valor":60},{"desc_medida":"BUSTO","tamanho":"G","valor":110},{"desc_medida":"BUSTO","tamanho":"GG","valor":114},{"desc_medida":"BUSTO","tamanho":"M","valor":104},{"desc_medida":"BUSTO","tamanho":"P","valor":100},{"desc_medida":"CINTURA","tamanho":"G","valor":110},{"desc_medida":"CINTURA","tamanho":"GG","valor":114},{"desc_medida":"CINTURA","tamanho":"M","valor":104},{"desc_medida":"CINTURA","tamanho":"P","valor":100},{"desc_medida":"COMPRIMENTO","tamanho":"G","valor":97},{"desc_medida":"COMPRIMENTO","tamanho":"GG","valor":98},{"desc_medida":"COMPRIMENTO","tamanho":"M","valor":96},{"desc_medida":"COMPRIMENTO","tamanho":"P","valor":95}]}

def read_json2():
    return {"header":["TAMANHO","OMBROS","BUSTO","CINTURA","COMPRIMENTO"],"rows":{"P":["P","60 cm","100 cm","100 cm","95 cm"],"M":["M","62 cm","104 cm","104 cm","96 cm"],"G":["G","63 cm","110 cm","110 cm","97 cm"],"GG":["GG","64 cm","114 cm","114 cm","98 cm"]}}

def build_header(data):
    headers = []
    headers.append('TAMANHO')

    if 'medidas' in data:
        for medida in data['medidas']:
            if medida['desc_medida'] not in headers:
                headers.append(medida['desc_medida'])

    return headers

def build_rows(data):
    rows = {}

    if 'medidas' in data:
        for medida in data['medidas']:
            if medida['tamanho'] not in rows:
                rows[medida['tamanho']] = [medida['tamanho'], ' '.join([str(medida['valor']), 'cm'])]
            else:
                rows[medida['tamanho']].append(' '.join([str(medida['valor']), 'cm']))

    return rows

def transform():
    json1 = read_json1()
    print("Old Json: %s" % json1)
    data_transformed = {}
    data_transformed["header"] = build_header(json1)
    data_transformed["rows"] = build_rows(json1)

    return data_transformed



data = transform()
print("Transformed JSON: %s" % data)
print(data)
print("Codigo fonte disponivel em https://bitbucket.org/geraldoandradee/desafio-amaro-python")
