#!/usr/bin/env python
#! -*- coding:utf-8 -*-
import json

def read_file(filename):
    f = open(filename, 'r')
    data = f.read()
    f.close()
    return data

def read_json1():
    return json.loads(read_file('json1.json'))

def read_json2():
    return json.loads(read_file('json2.json'))

def build_header(data):
    headers = []
    headers.append('TAMANHO')

    if 'medidas' in data:
        for medida in data['medidas']:
            if medida['desc_medida'] not in headers:
                headers.append(medida['desc_medida'])

    return headers

def build_rows(data):
    rows = {}

    if 'medidas' in data:
        for medida in data['medidas']:
            if medida['tamanho'] not in rows:
                rows[medida['tamanho']] = [medida['tamanho'], ' '.join([str(medida['valor']), 'cm'])]
            else:
                rows[medida['tamanho']].append(' '.join([str(medida['valor']), 'cm']))

    return rows

def transform():
    json1 = read_json1()
    print("Old Json: %s" % json1)
    data_transformed = {}
    data_transformed["header"] = build_header(json1)
    data_transformed["rows"] = build_rows(json1)

    return data_transformed


if __name__ == '__main__':
    data = transform()
    print("Transformed JSON: %s" % data)
    print(data)