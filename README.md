# Desafio Amaro

Desafio resolvido em python3 disponível em https://hackpad.com/AMARO-Teste-de-programao-7X4s2pdqIUT. Resolução em https://repl.it/BWQB.


## Rode os testes

Para rodar os testes é simples:

    python3 -m unittest discover
    
    
## Rode o script

    python3 desafio.py